﻿using System;

namespace Classes
{
    public class Car
    {
        public string Make;
        public string Model;

        public Car()
        {
            Make = "defmake";
            Model = "defmodel";
        }

        public Car(string Make, string Model)
        {
            this.Make = Make;
            this.Model = Model;
        }
        public void WriteName()
        {
            Console.WriteLine("The car is {0} {1}", Make, Model);
        }
    }
}