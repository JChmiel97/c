﻿namespace Classes.Math
{
    public class Calculator
    {
        public static int Add(int firstValue, int secondValue)
        {
            return firstValue + secondValue;
        }
        public static int Subtract(int firstValue, int secondValue)
        {
            return firstValue - secondValue;
        }
        public static int Multiply(int firstValue, int secondValue)
        {
            return firstValue * secondValue;
        }
        public static int Divide(int firstValue, int secondValue)
        {
            return firstValue / secondValue;
        }
    }
}
