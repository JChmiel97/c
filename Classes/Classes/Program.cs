﻿using System;
using Classes.Math;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            var porsche911 =new Car("Porsche", "911");
            porsche911.WriteName();
            var garage = new Car[5];
            for (var i = 0; i < garage.Length; i++)
            {
                garage[i] = new Car();
            }
            garage[0] = new Car("Lamborghini", "Murcielago");
            garage[1] = new Car("Alfa Romeo", "Giulia");
            garage[2] = new Car("Renault", "Megane");
            garage[3] = new Car("VW", "Golf");

            for (var i = 0; i < garage.Length; i++)
            {
                Console.WriteLine("Car no. {0} is {1} {2}", i , garage[i].Make, garage[i].Model);
            }

            var firstName = "Jakub";
            var lastName = "Chmiel";
            var fullName = string.Format("{0} {1}", firstName, lastName);
            Console.WriteLine("My name is: {0} \n", fullName);
            var order = @"Dear Sir,
Please look into the following files
C:\Users\Example\Documents\whatever.pdf
C:\Users\Example\Documents\something_else.doc";
            Console.WriteLine(order + "\n");
            Console.WriteLine("10 + 20 = {0}", Calculator.Add(10,20));
            Console.WriteLine("30 - 21 = {0}",  Calculator.Subtract(30,21));
            Console.WriteLine("21 * 20 = {0}", Calculator.Multiply(20,21));
            Console.WriteLine("20 / 10 = {0}", Calculator.Divide(20,10));

            var method = ShippingMethod.RegisteredAirMail;
            Console.WriteLine("\n" + (int)method);
            Console.WriteLine(method);

            var methodId = 3;
            Console.WriteLine((ShippingMethod)methodId);

            var methodName = "RegularAirMail";
            var shippingMethod = (ShippingMethod)Enum.Parse(typeof(ShippingMethod), methodName);
            Console.WriteLine(shippingMethod);
        }
    }
}
