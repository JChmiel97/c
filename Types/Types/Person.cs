﻿namespace Types
{
    public class Person
    {
        public int Age;
        public int Height;

        public Person()
        {
            Age = 0;
            Height = 0;
        }

        public Person(int Age, int Height)
        {
            this.Age = Age;
            this.Height = Height;
        }
        public void MakeOlder()
        {
            Age += 10;
            Height -= 2;
        }
    }

}