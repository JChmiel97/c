﻿//Learning the differences between reference types and value types
using System;

namespace Types
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 10;
            var cost = number;
            cost++;
            Console.WriteLine("number = {0} cost = {1}", number, cost);

            var array1 = new int[3] {1, 2, 3};
            var array2 = array1;
            array2[0] = 0;
            Console.WriteLine("array1[0] = {0} array2[0] = {1}", array1[0], array2[0]);

            var myself = new Person(21, 186);
            var myClone = myself;
            myClone.MakeOlder();
            Console.WriteLine("Me: {0}y/o , {1}cm", myself.Age, myself.Height);
            Console.WriteLine("My clone: {0}y/o , {1}cm", myClone.Age, myClone.Height);
        }
    }
}
