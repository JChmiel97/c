﻿//Learning and testing different kinds of variables

using System;

namespace Variable_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 100;
            float cost = 1.29f;
            double count = 120.2;
            decimal totalPrice = (decimal) (cost * count);
            char character = 'H';
            string lastName = "Chmiel";
            bool goAgain = true;

            Console.WriteLine("Int: {0}", number);
            Console.WriteLine("Float: {0}", cost);
            Console.WriteLine("Double: {0}", count);
            Console.WriteLine("Decimal: {0}", totalPrice.ToString("0.##"));
            Console.WriteLine("Char: {0}", character);
            Console.WriteLine("String: {0}", lastName);
            Console.WriteLine("Bool: {0}", goAgain);

            byte overflowedByte;
            overflowedByte = 255;
            overflowedByte = (byte) (overflowedByte + 1);
            Console.WriteLine("Overflowed byte: {0}", overflowedByte);

            const float Pi = 3.141593f;
            Console.WriteLine("Pi = {0} {1} {2} {3}", Pi.ToString("0"), Pi.ToString("0.##"), Pi.ToString("0.####"), Pi);

            Console.OutputEncoding = System.Text.Encoding.Unicode;
            const char CopyrightSymbol = '\u00a9';
            const char AnotherCopyrightSymbol = (char) 169;
            Console.WriteLine("Copyright: {0} {1}",CopyrightSymbol, AnotherCopyrightSymbol);
            
            overflowedByte = 255;
            checked
            {
                try
                {
                    overflowedByte = (byte)(overflowedByte + 1);
                }
                catch (Exception)
                {
                    Console.WriteLine("Overflowed the byte type. The operation cannot be completed.");
                }
                
            }
            Console.WriteLine("Overflowed byte: " + overflowedByte);

        }
    }
}
